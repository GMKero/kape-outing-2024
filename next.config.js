/**
 * @type {import('next').NextConfig}
 */
const config = {
  reactStrictMode: true,
  transpilePackages: [
    '@ant-design/icons'
  ]
}

module.exports = config
