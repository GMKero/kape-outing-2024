import { Typography, Row, Col, Card, Image, Grid } from "antd";

interface Props {
  images: string[];
}

export function ImageCollectionRender(props: Props) {
  const { images } = props;
  const screens = Grid.useBreakpoint();

  console.log(screens);

  return (
    <div>
      <div>
        <Row gutter={8}>
          {images.map((x, index) => {
            return (
              <Col xs={12} sm={8} md={6}>
                <Card size="small">
                  <div>
                    <Image
                      src={x}
                      key={x}
                      style={{
                        height: screens.md ? "200px" : "40vw",
                        width: screens.md ? "200px" : "40vw",
                        objectFit: "cover",
                        objectPosition: "center",
                      }}
                    />
                  </div>
                  <div>{index + 1}</div>
                </Card>
              </Col>
            );
          })}
        </Row>
      </div>
    </div>
  );
}
