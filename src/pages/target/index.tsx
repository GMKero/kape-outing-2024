import Head from "next/head";
import { Button, Col, Row, Space, Typography, Image, Card, Tabs } from "antd";
import { LeftOutlined } from "@ant-design/icons";
import { ImageCollectionRender } from "src/components/ImageCollectionRender";
import TabPane from "antd/es/tabs/TabPane";

const images_easy = [
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_easy_3.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_easy_5.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_easy_2.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_easy_9.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_easy_1.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_easy_3.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_easy_8.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_easy_5.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_easy_4.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_easy_6.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_easy_2.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_easy_1.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_easy_5.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_easy_4.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_easy_7.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_easy_7.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_easy_10.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_easy_3.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_easy_4.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_easy_8.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_easy_6.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_easy_9.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_easy_1.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_easy_7.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_easy_2.jpg",
];

const images_medium = [
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_medium_6.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_10.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_medium_7.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_12.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_medium_2.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_medium_3.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_6.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_13.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_15.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_14.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_3.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_medium_4.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_medium_4.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_medium_5.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_medium_6.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_medium_2.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_medium_1.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_2.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_medium_3.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_medium_8.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_1.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_9.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_17.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_19.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_7.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_18.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_medium_5.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_16.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_5.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_20.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_8.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_medium_1.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_medium_11.jpg",
];

const images_hard = [
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_hard_3.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_10.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_hard_1.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_7.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_17.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_19.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_9.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_11.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_hard_2.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/b_hard_1.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_3.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_13.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_16.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/a_hard_2.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_15.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_14.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_5.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_1.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_4.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_2.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_21.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_18.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_8.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_6.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_20.jpg",
  "https://gitlab.com/GMKero/kape-outing-2024/-/raw/master/src/image/c_hard_12.jpg",
];

export default function Home() {
  return (
    <div>
      <Head>
        <title>Kape Photo Hunt</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div
        style={{
          width: "100vw",
          minHeight: "100vh",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          padding: "16px",
          textAlign: "center",
        }}
      >
        <Row style={{ width: "100%" }}>
          <Col>
            <Button type="link" icon={<LeftOutlined />} href="/">
              Back
            </Button>
          </Col>
        </Row>
        <Typography.Title>Kape Photo Hunt</Typography.Title>
        <Typography.Title level={3}>Route A</Typography.Title>
        <Tabs
          defaultActiveKey="easy"
          items={[
            {
              label: "Easy",
              key: "easy",
              children: <ImageCollectionRender images={images_easy} />,
            },
            {
              label: "Medium",
              key: "medium",
              children: <ImageCollectionRender images={images_medium} />,
            },
            {
              label: "Hard",
              key: "hard",
              children: <ImageCollectionRender images={images_hard} />,
            },
          ]}
        ></Tabs>
      </div>
    </div>
  );
}
